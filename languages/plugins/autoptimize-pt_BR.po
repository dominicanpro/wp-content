# Translation of Plugins - Autoptimize - Stable (latest release) in Portuguese (Brazil)
# This file is distributed under the same license as the Plugins - Autoptimize - Stable (latest release) package.
msgid ""
msgstr ""
"PO-Revision-Date: 2017-12-31 19:30:03+0000\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: GlotPress/2.4.0-alpha\n"
"Language: pt_BR\n"
"Project-Id-Version: Plugins - Autoptimize - Stable (latest release)\n"

#: classlesses/autoptimizeExtra.php:313
msgid "Comma-separated list of local or 3rd party JS-files that should loaded with the <code>async</code> flag. JS-files from your own site will be automatically excluded if added here."
msgstr "Lista de arquivos JS locais ou de terceiros, separados por uma lista, que devem ser carregado com o parâmetro <code>async</code>. Arquivos JS de seu próprio site serão excluídos se adicioná-los aqui."

#: classlesses/autoptimizeExtra.php:309
msgid "Async Javascript-files <em>(advanced users)</em>"
msgstr "Arquivos JavaScript assíncronos <em>(usuários avançados)</em>"

#: classlesses/autoptimizeExtra.php:305
msgid "Add 3rd party domains you want the browser to <a href=\"https://www.keycdn.com/support/preconnect/#primary\" target=\"_blank\">preconnect</a> to, separated by comma's. Make sure to include the correct protocol (HTTP or HTTPS)."
msgstr "Adicione domínios de terceiros que você quer que o navegador use <a href=\"https://www.keycdn.com/support/preconnect/#primary\" target=\"_blank\">preconnect</a>, separado por vírgulas. Certifique-se de incluir o protocolo corretamente (HTTP ou HTTPS)."

#: classlesses/autoptimizeExtra.php:303
msgid "Preconnect to 3rd party domains <em>(advanced users)</em>"
msgstr "Usar \"Preconnect\" para domínios de terceiros <em>(usuários avançados)</em>"

#: classlesses/autoptimizeExtra.php:299
msgid "Combine and load fonts asynchronously with <a href=\"https://github.com/typekit/webfontloader#readme\" target=\"_blank\">webfont.js</a>"
msgstr "Combina e carrega fontes assincronamente com <a href=\"https://github.com/typekit/webfontloader#readme\" target=\"_blank\">webfont.js</a>"

#: classlesses/autoptimizeExtra.php:298
msgid "Combine and link in head"
msgstr "Combinar e linkar no \"head\""

#: classlesses/autoptimizeExtra.php:296
msgid "Leave as is"
msgstr "Deixar como está"

#: classlesses/autoptimizeExtra.php:294
msgid "Google Fonts"
msgstr "Fontes Google"

#: classlesses/autoptimizeExtra.php:290
msgid "Removing query strings (or more specificaly the <code>ver</code> parameter) will not improve load time, but might improve performance scores."
msgstr "Remover variáveis da url (ou mais especificamente parâmetro <code>ver</code> não vai melhorar o tempo de carregamento, mas pode melhorar notas de desempenho."

#: classlesses/autoptimizeExtra.php:288
msgid "Remove query strings from static resources"
msgstr "Remover variáveis da url de recursos estáticos"

#: classlesses/autoptimizeExtra.php:284
msgid "Removes WordPress' core emojis' inline CSS, inline JavaScript, and an otherwise un-autoptimized JavaScript file."
msgstr "Remove o CSS e JavaScript em linha dos emojis no arquivos básicos do WordPress e um arquivo não-autoptimizado"

#: classlesses/autoptimizeExtra.php:282
msgid "Remove emojis"
msgstr "Remove emojis"

#: classlesses/autoptimizeExtra.php:279
msgid "The following settings can improve your site's performance even more."
msgstr "As seguintes configurações podem melhorar o desempenho de seu site ainda mais."

#: classlesses/autoptimizeExtra.php:278
msgid "Extra Auto-Optimizations"
msgstr "Auto-Optimizações Extras"

#: classlesses/autoptimizeExtra.php:260
msgid "Extra"
msgstr "Extra"

#: classes/autoptimizeCLI.php:17
msgid "Cache flushed."
msgstr "Cache limpo."

#: classes/autoptimizeCLI.php:15
msgid "Flushing the cache..."
msgstr "Limpando o cache..."

#. #-#-#-#-#  autoptimize-code.pot (Autoptimize 2.3.1)  #-#-#-#-#
#. Plugin URI of the plugin/theme
#. #-#-#-#-#  autoptimize-code.pot (Autoptimize 2.3.1)  #-#-#-#-#
#. Author URI of the plugin/theme
msgid "https://autoptimize.com/"
msgstr "https://autoptimize.com/"

#: classes/autoptimizeToolbar.php:122
msgid "Your Autoptimize cache might not have been purged successfully, please check on the <a href=%s>Autoptimize settings page</a>."
msgstr "Seu cache Autoptimize pode não ter sido limpado com sucesso, por favor, verifique na <a href=%s>página de configuração do Autoptimize</a>."

#: classes/autoptimizeConfig.php:386
msgid "Need help? <a href='https://wordpress.org/plugins/autoptimize/faq/'>Check out the FAQ here</a>."
msgstr "Precise de ajuda? <a href='https://br.wordpress.org/plugins/autoptimize/faq/'>veja a FAQ aqui</a>."

#: classes/autoptimizeConfig.php:348
msgid "By default Autoptimize is also active on your shop's cart/ checkout, uncheck not to optimize those."
msgstr "Por padrão o Autoptimize é ativo também no checkout / carrinho de sua loja, desmarque isto para desativá-lo neles."

#: classes/autoptimizeConfig.php:346
msgid "Also optimize shop cart/ checkout?"
msgstr "também otimiza carrinho / checkout da loja?"

#: classes/autoptimizeConfig.php:339
msgid "By default Autoptimize is also active for logged on users, uncheck not to optimize when logged in e.g. to use a pagebuilder."
msgstr "Por padrão o Autoptimize também é ativo para usuários logados, desmarque para não otimização quando logado, por exemplo, num construtor de páginas."

#: classes/autoptimizeConfig.php:337
msgid "Also optimize for logged in users?"
msgstr "Também optimizar para usuários conectados?"

#: classes/autoptimizeConfig.php:328
msgid "Misc Options"
msgstr "Opções Diversas"

#: classes/autoptimizeConfig.php:321
msgid "%1$s files, totalling %2$s Kbytes (calculated at %3$s)"
msgstr "%1$s arquivos, totalizando %2$s Kbytes (calculado as %3$s)"

#: classes/autoptimizeConfig.php:300
msgid "Enter your CDN root URL to enable CDN for Autoptimized files. The URL can be http, https or protocol-relative (e.g. <code>//cdn.example.com/</code>). This is not needed for Cloudflare."
msgstr "Digite a raíz da sua CDN para ativar a CDN para arquivos Autoptimizados. A URL pode ser http, https ou relativos (e.g. <code>//cdn.exemplo.com/</code>). Isto não é necessário para Cloudflare."

#: classes/autoptimizeConfig.php:233
msgid "Let Autoptimize also extract JS from the HTML. <strong>Warning</strong>: this can make Autoptimize's cache size grow quickly, so only enable this if you know what you're doing."
msgstr "Deixe o Autoptimize também extrair Javascript do HTML. <strong>Aviso</strong>: isto pode fazer o tamanho do cache do Autoptimize crescer rapidamente, então ative apenas se você sabe o que está fazendo."

#: classes/autoptimizeConfig.php:225 classes/autoptimizeConfig.php:262
msgid "(deprecated)"
msgstr "(obsoleto)"

#: classes/autoptimizeConfig.php:221
msgid "Load JavaScript early, this can potentially fix some JS-errors, but makes the JS render blocking."
msgstr "carregar JavaScript mais cedo, isto tem o potencial de corrigir alguns erros de JS, mas tornar o JS bloqueador de renderização."

#: classes/autoptimizeConfig.php:162
msgid "<strong>You are using a very old version of PHP</strong> (5.2.x or older) which has <a href=%s>serious security and performance issues</a>. Support for PHP 5.5 and below will be removed in one of the next AO released, please ask your hoster to provide you with an upgrade path to 7.x."
msgstr "<strong>Você está usando uma versão PHP muito mais velha</strong> (5.2.x ou mais velha) que tem <a href=%s>questões sérias de performance e segurança</a>. O suporte ao PHP 5.5 ou mais baixos serão removidos na próxima versão do AO, por favor, solicite à sua hospedagem para atualizar para 7.x."

#: autoptimize.php:120
msgid "Autoptimize cannot write to the cache directory (%s), please fix to enable CSS/ JS optimization!"
msgstr "Autoptimize não pode gravar ao diretório do cache (%s), por favor corrija para ativar otimização do CSS/JS!"

#: classlesses/autoptimizePartners.php:87
msgid "Have a look at <a href=\"http://optimizingmatters.com/\">optimizingmatters.com</a> for Autoptimize power-ups!"
msgstr "Dê uma olhada em <a href=\"http://optimizingmatters.com/\">optimizingmatters.com</a> para poderes extras do Autoptimize!"

#: classlesses/autoptimizePartners.php:77
msgid "These Autoptimize power-ups and related services will improve your site's performance even more!"
msgstr "Estes \"poderes extras\" do Autoptimize e serviços relacionados vão aumentar ainda mais o desempenho de seu site!"

#: classlesses/autoptimizePartners.php:16
msgid "Optimize More!"
msgstr "Otimize Mais"

#: classlesses/autoptimizeCacheChecker.php:45
msgid "Autoptimize's cache size is getting big, consider purging the cache. Have a look at https://wordpress.org/plugins/autoptimize/faq/ to see how you can keep the cache size under control."
msgstr "O cache do Autoptimize está ficando grande, considere limpar o cache. Verifique a https://br.wordpress.org/plugins/autoptimize/faq/ para ver como manter o tamanho de seu cache sob controle."

#: classlesses/autoptimizeCacheChecker.php:44
msgid "Autoptimize cache size warning"
msgstr "Aviso do tamanho do cache do Autoptimize"

#: classes/autoptimizeToolbar.php:123
msgid "Dismiss this notice."
msgstr "Fechar este aviso"

#: classes/autoptimizeToolbar.php:96
msgid "Delete Cache"
msgstr "Limpar Cache"

#: classes/autoptimizeToolbar.php:88
msgid "Files"
msgstr "Arquivos"

#: classes/autoptimizeToolbar.php:87
msgid "Size"
msgstr "Tamanho"

#: classes/autoptimizeConfig.php:688
msgid "Main"
msgstr "Principal"

#: classes/autoptimizeConfig.php:206
msgid "Enable this if you want HTML comments to remain in the page."
msgstr "Habilite isto se você deseja que comentários HTML permaneçam na página."

#: classlesses/autoptimizeCacheChecker.php:61
msgid "<strong>Autoptimize's cache size is getting big</strong>, consider purging the cache. Have a look at <a href=\"https://wordpress.org/plugins/autoptimize/faq/\" target=\"_blank\">the Autoptimize FAQ</a> to see how you can keep the cache size under control."
msgstr "<strong>O cache do Autoptimize está ficando grande</strong>, considere limpar o cache. Verifique a <a href=\"https://br.wordpress.org/plugins/autoptimize/faq/\" target=\"_blank\">FAQ do Autoptimize</a> para ver como manter o tamanho de seu cache sob controle."

#: classes/autoptimizeConfig.php:675
msgid "Posted %s"
msgstr "Publicado %s"

#: classes/autoptimizeConfig.php:670
msgid "No items"
msgstr "Nenhum ítem"

#: classes/autoptimizeConfig.php:387
msgid "Try my other plugins!"
msgstr "Experimente meus outros plugins!"

#: classes/autoptimizeConfig.php:387
msgid "Happy with Autoptimize?"
msgstr "Feliz com Autoptimize?"

#: classes/autoptimizeConfig.php:270
msgid "Check this option for Autoptimize to also aggregate CSS in the HTML."
msgstr "Marque esta opção para que o Autoptimize também agregue o CSS no HTML."

#: classes/autoptimizeConfig.php:268
msgid "Also aggregate inline CSS?"
msgstr "Também agregar o CSS em linha?"

#: classlesses/autoptimizeExtra.php:297
msgid "Remove Google Fonts"
msgstr "Remover fontes Google"

#: classes/autoptimizeConfig.php:231
msgid "Also aggregate inline JS?"
msgstr "Também agregar JS em linha?"

#. Author of the plugin/theme
msgid "Frank Goossens (futtta)"
msgstr "Frank Goossens (futtta)"

#. Description of the plugin/theme
msgid "Optimizes your website, concatenating the CSS and JavaScript code, and compressing it."
msgstr "Otimiza seu site, concatenando o código CSS e JavaScript, além de compactá-lo."

#: classes/autoptimizeConfig.php:599 classes/autoptimizeConfig.php:605
msgid "Settings"
msgstr "Configurações"

#: classes/autoptimizeConfig.php:550
msgid "Autoptimize Options"
msgstr "Opções do Autoptimize"

#: classes/autoptimizeConfig.php:411
msgid "Do not donate for this plugin!"
msgstr "Não faça doações a este plugin!"

#: classes/autoptimizeConfig.php:396
msgid "Web Technology"
msgstr "Tecnologia Web"

#: classes/autoptimizeConfig.php:395
msgid "WordPress"
msgstr "WordPress"

#. #-#-#-#-#  autoptimize-code.pot (Autoptimize 2.3.1)  #-#-#-#-#
#. Plugin Name of the plugin/theme
#: classes/autoptimizeConfig.php:394 classes/autoptimizeToolbar.php:69
msgid "Autoptimize"
msgstr "Autoptimize"

#: classes/autoptimizeConfig.php:392
msgid "futtta about"
msgstr "sobre futta"

#: classes/autoptimizeConfig.php:363
msgid "Save Changes and Empty Cache"
msgstr "Salvar Mudanças e Esvaziar Cache"

#: classes/autoptimizeConfig.php:362
msgid "Save Changes"
msgstr "Salvar Mudanças"

#: classes/autoptimizeConfig.php:333
msgid "By default files saved are static css/js, uncheck this option if your webserver doesn't properly handle the compression and expiry."
msgstr "Por padrão os arquivos são salvos como css/js estáticos, desmarque esta opção se seu servidor web não lida apropriadamente com compressão e expiração."

#: classes/autoptimizeConfig.php:331
msgid "Save aggregated script/css as static files?"
msgstr "Salvar scripts/CSS agregados como arquivos estáticos?"

#: classes/autoptimizeConfig.php:317
msgid "Cached styles and scripts"
msgstr "Estilos e scripts no cache"

#: classes/autoptimizeConfig.php:314
msgid "No"
msgstr "Não"

#: classes/autoptimizeConfig.php:314
msgid "Yes"
msgstr "Sim"

#: classes/autoptimizeConfig.php:313
msgid "Can we write?"
msgstr "Podemos gravar?"

#: classes/autoptimizeConfig.php:309
msgid "Cache folder"
msgstr "Diretório do cache"

#: classes/autoptimizeConfig.php:306 classes/autoptimizeToolbar.php:77
msgid "Cache Info"
msgstr "Informação do Cache"

#: classes/autoptimizeConfig.php:298
msgid "CDN Base URL"
msgstr "URL base da CDN"

#: classes/autoptimizeConfig.php:295
msgid "CDN Options"
msgstr "Opções de CDN"

#: classes/autoptimizeConfig.php:289
msgid "A comma-separated list of CSS you want to exclude from being optimized."
msgstr "Uma lista de CSS, separados por vírgula, que você deseja evitar que sejam otimizados."

#: classes/autoptimizeConfig.php:287
msgid "Exclude CSS from Autoptimize:"
msgstr "Excluir CSS do Autoptimize:"

#: classes/autoptimizeConfig.php:284
msgid "Inlining all CSS can improve performance for sites with a low pageviews/ visitor-rate, but may slow down performance otherwise."
msgstr "Colocar o CSS inline pode melhorar a performance de sites com baixa taxa de pageviews/visitantes, mas pode diminuir a performance de sites maiores. "

#: classes/autoptimizeConfig.php:282
msgid "Inline all CSS?"
msgstr "Colocar todo o CSS inline?"

#: classes/autoptimizeConfig.php:279
msgid "Paste the above the fold CSS here."
msgstr "Cole o CSS \"acima da dobra\" aqui."

#: classes/autoptimizeConfig.php:275
msgid "Inline \"above the fold CSS\" while loading the main autoptimized CSS only after page load. <a href=\"http://wordpress.org/plugins/autoptimize/faq/\" target=\"_blank\">Check the FAQ</a> before activating this option!"
msgstr "Coloca o CSS \"Acima da Borda\" inline, enquanto carrega o CSS principal autoptimizado só após a página carregar. <a href=\"http://wordpress.org/plugins/autoptimize/faq/\" target=\"_blank\">Leia o FAQ</a> (<em>deferring CSS</em>) antes de ativar essa opção!"

#: classes/autoptimizeConfig.php:273
msgid "Inline and Defer CSS?"
msgstr "CSS em linha e adiado?"

#: classes/autoptimizeConfig.php:264
msgid "Don't autoptimize CSS outside the head-section. If the cache gets big, you might want to enable this."
msgstr "Não otimizar CSS fora da seção head. Se o cache ficar muito grande, você pode habilitar essa opção."

#: classes/autoptimizeConfig.php:262
msgid "Look for styles only in &lt;head&gt;?"
msgstr "Buscar estilos somente na seção &lt;head&gt;?"

#: classes/autoptimizeConfig.php:258
msgid "Enable this to include small background-images in the CSS itself instead of as separate downloads."
msgstr "Habilite isto para incluir imagens de fundo pequenas no próprio CSS ao invés de downloads separados."

#: classes/autoptimizeConfig.php:256
msgid "Generate data: URIs for images?"
msgstr "Gerar dados: URIs de imagens?"

#: classes/autoptimizeConfig.php:252
msgid "Optimize CSS Code?"
msgstr "Otimizar código CSS?"

#: classes/autoptimizeConfig.php:249
msgid "CSS Options"
msgstr "Opções de CSS"

#: classes/autoptimizeConfig.php:243
msgid "If your scripts break because of a JS-error, you might want to try this."
msgstr "Se seus scripts quebram por causa de um erro de JS, talvez queira usar isto."

#: classes/autoptimizeConfig.php:241
msgid "Add try-catch wrapping?"
msgstr "Adicionar encapsulamento \"try-catch\"?"

#: classes/autoptimizeConfig.php:238
msgid "A comma-separated list of scripts you want to exclude from being optimized, for example 'whatever.js, another.js' (without the quotes) to exclude those scripts from being aggregated and minimized by Autoptimize."
msgstr "Uma lista de scripts, separados por vírgulas, que você deseja excluir da otimização, por exemplo 'qualquercoisa.js, outro.js' (sem os apóstrofes) para evitar que estes scripts sejam agregados e minimizados pelo Autoptimize."

#: classes/autoptimizeConfig.php:236
msgid "Exclude scripts from Autoptimize:"
msgstr "Excluir scripts do Autoptimize:"

#: classes/autoptimizeConfig.php:227
msgid "Mostly useful in combination with previous option when using jQuery-based templates, but might help keeping cache size under control."
msgstr "Normalmente usado em conjunto com a opção anterior quando se usa temas baseados em jQuery, mas também pode ajudar a manter o tamanho do cache sob controle."

#: classes/autoptimizeConfig.php:225
msgid "Look for scripts only in &lt;head&gt;?"
msgstr "Buscar scripts somente na seção &lt;head&gt;?"

#: classes/autoptimizeConfig.php:219
msgid "Force JavaScript in &lt;head&gt;?"
msgstr "Forçar JavaScript na seção &lt;head&gt;?"

#: classes/autoptimizeConfig.php:215
msgid "Optimize JavaScript Code?"
msgstr "Otimizar código JavaScript?"

#: classes/autoptimizeConfig.php:212
msgid "JavaScript Options"
msgstr "Opções de JavaScript"

#: classes/autoptimizeConfig.php:204
msgid "Keep HTML comments?"
msgstr "Manter comentários HTML?"

#: classes/autoptimizeConfig.php:200
msgid "Optimize HTML Code?"
msgstr "Otimizar código HTML?"

#: classes/autoptimizeConfig.php:197
msgid "HTML Options"
msgstr "Opções de HTML"

#: classes/autoptimizeConfig.php:173 classes/autoptimizeConfig.php:180
msgid "Hide advanced settings"
msgstr "Ocultar configurações avançadas"

#: classes/autoptimizeConfig.php:172 classes/autoptimizeConfig.php:179
msgid "Show advanced settings"
msgstr "Exibir configurações avançadas"

#: classes/autoptimizeConfig.php:167 classlesses/autoptimizeExtra.php:274
#: classlesses/autoptimizePartners.php:74
msgid "Autoptimize Settings"
msgstr "Configurações do Autoptimize"

#: autoptimize.php:114
msgid "Autoptimize has just been updated. Please <strong>test your site now</strong> and adapt Autoptimize config if needed."
msgstr "Autoptimize foi atualizado. Por favor, <strong>teste seu site agora</strong> e adapte as configurações do Autoptimize se necessário."

#: autoptimize.php:108
msgid "Thank you for installing and activating Autoptimize. Please configure it under \"Settings\" -> \"Autoptimize\" to start improving your site's performance."
msgstr "Obrigado por instalar e ativar o Autoptimize! Por favor, configure o plugin em \"Configurações\" -> \"Autoptimize\" para melhorar a performance do seu site."